# Jeopardy Music on Demand
A simple and easy way to get Jeopardy music on demand.

# Why?
Becuase I listen to it a lot in my life.

# Libraries?
PyGame/appJar. PyGame for audio, appJar for the GUI. Install with PIP.

# How do I get the sound files?
I gitignored them, but here are the YouTube videos that I'm using.

https://www.youtube.com/watch?v=HuABhumm6fY - 15 mins

https://www.youtube.com/watch?v=rTyN-vvFIkE - 1 hour

https://www.youtube.com/watch?v=VBlFHuCzPgY - 1 hour elevator music

Convert them using a YouTube to MP3 converter. I personally use youtube2mp3.cc, but use whatever you wish to use.

Name the files as listed below once you've downloaded them:

15 mins - 15mins_jeopardy.mp3

1 hour - 1hr_jeopardy.mp3

1 hour elevator music - 1hr_elevator.mp3

And that's it!

It doesn't look pretty, but it wasn't meant to.
